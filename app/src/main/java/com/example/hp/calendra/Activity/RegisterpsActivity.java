package com.example.hp.calendra.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hp.calendra.Helper.AppConfig;
import com.example.hp.calendra.Model.DataRegister;
import com.example.hp.calendra.Model.DataRegisterPubliser;
import com.example.hp.calendra.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

public class RegisterpsActivity extends AppCompatActivity {
    ProgressDialog progress;
    Gson gson;
    RequestQueue requestQueue;
    EditText username, email, password, nomorhp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registerps);

        Button button = (Button)findViewById(R.id.buttonLoginPubliser);
        username = (EditText)findViewById(R.id.editTextUsernameRegisterPubliser);
        nomorhp = (EditText)findViewById(R.id.editTextNomorRegisterPubliser);
        email = (EditText)findViewById(R.id.editTextEmailRegisterPubliser);
        password = (EditText)findViewById(R.id.editTextPasswordRegisterPubliser);

        progress=new ProgressDialog(RegisterpsActivity.this);
        progress.setMessage("Please Wait...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setProgress(0);
        progress.setCanceledOnTouchOutside(false);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.show();
                SignupPublisher();
            }
        });
    }

    private void SignupPublisher() {
        requestQueue = Volley.newRequestQueue(RegisterpsActivity.this);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();

        String url = AppConfig.registerpubliser;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        progress.hide();
                        Log.d("Response", response);
                        RegisterpsActivity.ResponPublisherRegister posts = new RegisterpsActivity.ResponPublisherRegister();
                        try {
                            posts = gson.fromJson(response, RegisterpsActivity.ResponPublisherRegister.class);
                        }catch (Exception e){
                            Log.d("lihat respon", "Respon gagal");
                        }
                        if(posts.getStatus().equalsIgnoreCase("success")){
                            Intent i = new Intent(RegisterpsActivity.this, LoginpsActivity.class);
                            startActivity(i);
                        }else{
                            Toast.makeText(RegisterpsActivity.this,"Tidak bisa", Toast.LENGTH_SHORT).show();
                            Log.d("Response :",response);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("nama", username.getText().toString());
                params.put("email", email.getText().toString());
                params.put("phone", nomorhp.getText().toString());
                params.put("password", password.getText().toString());
                return params;
            }
        };
        requestQueue.add(postRequest);
    }

    public class ResponPublisherRegister{
        private String status;
        private String message;
        private DataRegisterPubliser data;

        public ResponPublisherRegister(String status, String message, DataRegisterPubliser data) {
            this.status = status;
            this.message = message;
            this.data = data;
        }

        public ResponPublisherRegister() {

        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public DataRegisterPubliser getData() {
            return data;
        }

        public void setData(DataRegisterPubliser data) {
            this.data = data;
        }
    }
}
