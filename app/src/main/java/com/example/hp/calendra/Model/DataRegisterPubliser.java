package com.example.hp.calendra.Model;

/**
 * Created by hp on 10/12/2017.
 */

public class DataRegisterPubliser {
    private String emial, name, phone, password, id_user;
    private int id_Publisher;

    public DataRegisterPubliser(String emial, String name, String phone, String password, String id_user, int id_Publisher) {
        this.emial = emial;
        this.name = name;
        this.phone = phone;
        this.password = password;
        this.id_user = id_user;
        this.id_Publisher = id_Publisher;
    }

    public String getEmial() {
        return emial;
    }

    public void setEmial(String emial) {
        this.emial = emial;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public int getId_Publisher() {
        return id_Publisher;
    }

    public void setId_Publisher(int id_Publisher) {
        this.id_Publisher = id_Publisher;
    }
}
