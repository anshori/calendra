package com.example.hp.calendra.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.hp.calendra.Adapter.CategoryAdapter;
import com.example.hp.calendra.Adapter.EventAdapter;
import com.example.hp.calendra.Model.Category;
import com.example.hp.calendra.R;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity {

    ArrayList<Category> categories = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Category category = new Category();
        category.setName("seminar");
        categories.add(category);
        category = new Category();
        category.setName("workshop");
        category = new Category();
        category.setName("konferensi");
        category = new Category();
        category.setName("pameran");
        categories.add(category);
        category = new Category();
        category.setName("lomba");
        categories.add(category);
        category = new Category();
        category.setName("training");
        categories.add(category);
        category = new Category();
        category.setName("kajian");
        categories.add(category);
        category = new Category();
        category.setName("konser");
        categories.add(category);

        RecyclerView mEventRecycle = (RecyclerView) findViewById(R.id.rcv_Category);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mEventRecycle.setLayoutManager(layoutManager);
        mEventRecycle.setAdapter(new CategoryAdapter(categories, this));
    }
}
