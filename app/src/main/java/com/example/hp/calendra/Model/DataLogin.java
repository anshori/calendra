package com.example.hp.calendra.Model;

/**
 * Created by hp on 10/12/2017.
 */

public class DataLogin {
    private int id;
    private String username;
    private String token;
    private int role;

    public DataLogin(int id, String username, String token, int role) {
        this.id = id;
        this.username = username;
        this.token = token;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
