package com.example.hp.calendra.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hp.calendra.Helper.AppConfig;
import com.example.hp.calendra.Helper.SharedPref;
import com.example.hp.calendra.Model.DataLogin;
import com.example.hp.calendra.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

public class LogincsActivity extends AppCompatActivity {
    EditText editTextUsername, editTextPassword;
    ProgressBar progressBar;
    RequestQueue requestQueue;
    ProgressDialog progressDialog;
    Button btnLoginCostumer;
    Gson gson;
    SharedPref sharedPref;
    String uname, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logincs);

        sharedPref = new SharedPref(this);
        uname = sharedPref.loadData("username");
        password = sharedPref.loadData("password");
        if (!uname.equalsIgnoreCase("")){
            costumerLogin();
        }

        progressBar = (ProgressBar) findViewById(R.id.progressBarCostumer);
        editTextUsername = (EditText) findViewById(R.id.editTextUsernameCostumer);
        editTextPassword = (EditText) findViewById(R.id.editTextPasswordCostumer);

        progressDialog=new ProgressDialog(LogincsActivity.this);
        progressDialog.setMessage("Mohon Bersabar...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgress(0);
        progressDialog.setCanceledOnTouchOutside(false);

        findViewById(R.id.buttonLoginCostumer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
//                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                uname = editTextUsername.getText().toString();
                password = editTextPassword.getText().toString();
                costumerLogin();
            }
        });

        findViewById(R.id.textViewRegisterCostumer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(getApplicationContext(), RegistercsActivity.class));
            }
        });
    }

    private void costumerLogin() {

        //validating inputs
        if (TextUtils.isEmpty(uname)) {
            editTextUsername.setError("Mohon masukkan username");
            editTextUsername.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            editTextPassword.setError("Mohon masukkan password");
            editTextPassword.requestFocus();
            return;
        }

        requestQueue = Volley.newRequestQueue(LogincsActivity.this);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();

        String url = AppConfig.logincustomer;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response",response);
                        progressDialog.hide();
                        ResponLoginCostumer posts =  new ResponLoginCostumer();
                        try{
                            posts =  gson.fromJson(response, ResponLoginCostumer.class);
                        }catch(Exception e){
                            Log.d("Response", "Gson gagal");
                            Toast.makeText(LogincsActivity.this, "Wrong Username/Password !!!!", Toast.LENGTH_SHORT).show();
                        }
                        if(posts.getStatus()!=null)
                            if(posts.getStatus().equalsIgnoreCase("success")){
                                sharedPref.saveData("username",uname);
                                sharedPref.saveData("password" ,password);
                                Intent i = new Intent(LogincsActivity.this, MainActivity.class);
                                startActivity(i);
                            }else{
                                Toast.makeText(LogincsActivity.this, "Tidak bisa login", Toast.LENGTH_SHORT).show();
                                Log.d("Response", response);
                            }
                        else
                            Toast.makeText(LogincsActivity.this, "Failed Login", Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Response", error.toString());
                        Toast.makeText(LogincsActivity.this, "error listener", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", uname);
                params.put("password", password);
                return params;
            }
        };
        requestQueue.add(postRequest);
    }

    public class ResponLoginCostumer {
        private String status;
        private String message;
        private DataLogin data;

        public ResponLoginCostumer(String status, String message, DataLogin data) {
            this.status = status;
            this.message = message;
            this.data = data;
        }

        public ResponLoginCostumer() {

        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public DataLogin getData() {
            return data;
        }

        public void setData(DataLogin data) {
            this.data = data;
        }
    }
}
