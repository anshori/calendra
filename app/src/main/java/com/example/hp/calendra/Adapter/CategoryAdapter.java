package com.example.hp.calendra.Adapter;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hp.calendra.Activity.DetailEventActivity;
import com.example.hp.calendra.Model.Category;
import com.example.hp.calendra.Model.ModelEvent;
import com.example.hp.calendra.R;

import java.util.List;

/**
 * Created by Reaper on 12/4/2017.
 */


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ClientViewHolder> {

    private List Client;
    final Context context;

    public CategoryAdapter(List<Category> listCart, Context context) {
        this.Client = (List) listCart;
        this.context = (Context) context;
    }



    public class ClientViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvDescription;

        ClientViewHolder(View v) {
            super(v);
            tvTitle = (TextView) v.findViewById(R.id.tv_itemCategory);
        }
    }

    @Override
    public CategoryAdapter.ClientViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_category, viewGroup, false);
        CategoryAdapter.ClientViewHolder pvh = new CategoryAdapter.ClientViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final CategoryAdapter.ClientViewHolder listViewHolder, final int position) {
        final Category modelEvent = (Category) Client.get(position);
        listViewHolder.tvTitle.setText(modelEvent.getName());
    }

    @Override
    public int getItemCount() {
        return Client.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
