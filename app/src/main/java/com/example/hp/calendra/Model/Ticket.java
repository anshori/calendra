package com.example.hp.calendra.Model;

/**
 * Created by Reaper on 12/7/2017.
 */

public class Ticket {
    String price;
    String name;
    String description;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
