package com.example.hp.calendra;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

public class PometraActivity extends AppCompatActivity implements View.OnClickListener{

    EditText etTujuan, etContent;
    Button btnSend;
    String tujuan,content,token;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pometra);
        //Init
        gson = new GsonBuilder().create();

        etTujuan = (EditText)findViewById(R.id.pometra_tujuan);
        etContent = (EditText)findViewById(R.id.pometra_content);
        btnSend = (Button) findViewById(R.id.btn_pometra_send);
        btnSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_pometra_send:
                tujuan = etTujuan.getText().toString();
                content = etContent.getText().toString();
                RequestQueue queue = Volley.newRequestQueue(PometraActivity.this);
                String url ="https://api.pometera.id/smsnotif/token";

                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Display the first 500 characters of the response string.
                                Log.d("Response",response);
                                Respon respon = gson.fromJson(response,Respon.class);
                                token = respon.getAccess_token();
                                sendMessage();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Response Error",error.toString());
                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String,String> params = new HashMap<>();
                        params.put("Content-Type","application/x-www-form-urlencoded");
                        params.put("X-Pometera-Api-Key","f1f8f4bf-b777-4cae-bc3f-204f1bcecc75");
                        return params;
                    }
                    @Override
                    public Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String,String> params = new HashMap<>();
                        params.put("grant_type","client_credentials");
                        return params;
                    }
                };
                // Add the request to the RequestQueue.
                queue.add(stringRequest);

        }
    }

    public void sendMessage(){
        RequestQueue queue = Volley.newRequestQueue(PometraActivity.this);
        String url ="https://api.pometera.id/smsnotif/messages";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("Response",response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Response Error",error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> params = new HashMap<>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                params.put("X-Pometera-Api-Key","f1f8f4bf-b777-4cae-bc3f-204f1bcecc75");
                params.put("Accept","application/json");
                params.put("Authorization","Bearer "+token);
                return params;
            }
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params = new HashMap<>();
                params.put("msisdn",tujuan);
                params.put("content",content);
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public class Respon{
        String scope;
        String token_type;
        String expire_in;
        String access_token;

        public String getScope() {
            return scope;
        }

        public void setScope(String scope) {
            this.scope = scope;
        }

        public String getToken_type() {
            return token_type;
        }

        public void setToken_type(String token_type) {
            this.token_type = token_type;
        }

        public String getExpire_in() {
            return expire_in;
        }

        public void setExpire_in(String expire_in) {
            this.expire_in = expire_in;
        }

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }
    }
}
