package com.example.hp.calendra.Activity;

import android.app.usage.UsageEvents;
import android.content.Intent;
import android.provider.CalendarContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.hp.calendra.Adapter.EventAdapter;
import com.example.hp.calendra.Helper.AppConfig;
import com.example.hp.calendra.Model.ModelEvent;
import com.example.hp.calendra.Pometera;
import com.example.hp.calendra.R;
import com.example.hp.calendra.Model.Ticket;
import com.example.hp.calendra.Adapter.TicketAdapter;
import com.example.hp.calendra.Model.Timeline;
import com.example.hp.calendra.Adapter.TimelineAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class DetailEventActivity extends AppCompatActivity implements View.OnClickListener{

    ArrayList<Ticket> tickets = new ArrayList<>();
    ArrayList<Timeline> timelines = new ArrayList<>();
    ModelEvent modelEvent = new ModelEvent();
    String title = "", description = "";
    RequestQueue requestQueue;
    String id = "";
    RecyclerView rcvTicket, rcvTimeline;
    ImageView imgPoster;
    TextView txtDesc, txtTitle;

    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_event);
        gson = new GsonBuilder().create();


        imgPoster = (ImageView) findViewById(R.id.posterDetail);
        txtDesc = (TextView) findViewById(R.id.tv_detailEvent_description);
        txtTitle = (TextView) findViewById(R.id.tv_detailEvent_title);

        requestQueue = Volley.newRequestQueue(this);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                title= null;
                description = null;
            } else {
                title= extras.getString("title");
                description= extras.getString("description");
                id = extras.getString("id");
            }
        } else {
            title= (String) savedInstanceState.getSerializable("STRING_I_NEED");
        }
        Log.d("Get id",id);
        TextView tvTitle = (TextView) findViewById(R.id.tv_detailEvent_title);
        tvTitle.setText(title);

        TextView tvDescription = (TextView) findViewById(R.id.tv_detailEvent_description);
        tvDescription.setText(description);

        //Init view
        Button btnAdd = (Button)findViewById(R.id.btn_detail_add);
        btnAdd.setOnClickListener(this);

        rcvTicket = (RecyclerView) findViewById(R.id.rcvTicket);
        rcvTicket.setLayoutManager(new LinearLayoutManager(this));
        rcvTicket.setAdapter(new TicketAdapter(tickets,this));

        rcvTimeline = (RecyclerView) findViewById(R.id.rcvTimeline);
        rcvTimeline.setLayoutManager(new LinearLayoutManager(this));
        rcvTimeline.setAdapter(new TimelineAdapter(timelines,this));

        loadVolley();
    }

    public void loadVolley(){
        //LOAD TIMELINE
        StringRequest postRequest = new StringRequest(Request.Method.GET, AppConfig.getTimeline+id,
                new com.android.volley.Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        ResponseThis r = gson.fromJson(response,ResponseThis.class);
                        timelines = r.getData();
                        rcvTimeline.setAdapter(new TimelineAdapter(timelines,DetailEventActivity.this));
                    }
                },
                new com.android.volley.Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        requestQueue.add(postRequest);
        //LOAD TICKET
        StringRequest postRequest2 = new StringRequest(Request.Method.GET, AppConfig.getTicket+id,
                new com.android.volley.Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        ResponseTicket r = gson.fromJson(response,ResponseTicket.class);
                        tickets = r.getData();
                        rcvTicket.setAdapter(new TicketAdapter(tickets,DetailEventActivity.this));
                    }
                },
                new com.android.volley.Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        requestQueue.add(postRequest2);
        //LOAD EVENT
        StringRequest postRequest3 = new StringRequest(Request.Method.GET, AppConfig.getEventDetail+id,
                new com.android.volley.Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        ResponseEvent r = gson.fromJson(response,ResponseEvent.class);
                        ModelEvent event = r.getData().get(0);
                        Glide.with(DetailEventActivity.this).load(AppConfig.IP+event.getImage()).into(imgPoster);
                        txtTitle.setText(event.getTittle());
                        txtDesc.setText(event.getDescription());
                    }
                },
                new com.android.volley.Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        requestQueue.add(postRequest3);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_detail_add:
                Calendar beginTime = Calendar.getInstance();
                beginTime.set(2017, 12, 19, 0, 1);
                Calendar endTime = Calendar.getInstance();
                endTime.set(2017, 12, 23, 23, 59);

                Intent intent = new Intent(Intent.ACTION_INSERT)
                        .setData(CalendarContract.Events.CONTENT_URI)
                        .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                        .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                        .putExtra(CalendarContract.Events.TITLE, title)
                        .putExtra(CalendarContract.Events.DESCRIPTION, description)
                        .putExtra(CalendarContract.Events.EVENT_LOCATION, "https://www.google.co.id/maps/place/Gedung+Creative+-+Digital+Hub+(Gerdhu)/@-7.2941564,112.7749311,17z/data=!3m1!4b1!4m5!3m4!1s0x2dd7fa4a44224495:0xe533538adea6b2e0!8m2!3d-7.2941564!4d112.7771198?hl=id")
                        .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                        .putExtra(CalendarContract.Events.EVENT_COLOR, "Flamingo")
                        .putExtra(Intent.EXTRA_EMAIL, "febilentinjyg@gmail.com,ricoarisandyw@gmail.com");
                startActivity(intent);

                Pometera pometera = new Pometera(this);
                String message ="CALENDRA REMINDER!!!\n"+ title + "\nDescription :"+description;
                pometera.sendMessage("6281331653552",message);
        }
    }

    public class ResponseThis{
        String status;
        ArrayList<Timeline> data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public ArrayList<Timeline> getData() {
            return data;
        }

        public void setData(ArrayList<Timeline> data) {
            this.data = data;
        }
    }

    public class ResponseTicket{
        String status;
        ArrayList<Ticket> data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public ArrayList<Ticket> getData() {
            return data;
        }

        public void setData(ArrayList<Ticket> data) {
            this.data = data;
        }
    }

    public class ResponseEvent{

        String status;
        ArrayList<ModelEvent> data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public ArrayList<ModelEvent> getData() {
            return data;
        }

        public void setData(ArrayList<ModelEvent> data) {
            this.data = data;
        }
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

}
