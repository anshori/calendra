package com.example.hp.calendra.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.hp.calendra.Activity.DetailEventActivity;
import com.example.hp.calendra.Activity.MainActivity;
import com.example.hp.calendra.Helper.AppConfig;
import com.example.hp.calendra.Model.ModelEvent;
import com.example.hp.calendra.R;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Reaper on 12/4/2017.
 */


public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ClientViewHolder> {

    private List Client;
    final Context context;
    private MediaPlayer mediaPlayer = new MediaPlayer();
    int[] idImage = {R.drawable.poster1,R.drawable.poster2,R.drawable.poster3};

    public EventAdapter(List<ModelEvent> listCart, Context context) {
        this.Client = (List) listCart;
        this.context = (Context) context;
    }



    public class ClientViewHolder extends RecyclerView.ViewHolder {
        LinearLayout cardView;
        TextView tvTitle, tvDescription;
        ImageView imageView;

        ClientViewHolder(View v) {
            super(v);
            cardView = (LinearLayout) v.findViewById(R.id.cv_itemEvent);
            tvTitle = (TextView) v.findViewById(R.id.tv_itemEvent_title);
            tvDescription = (TextView) v.findViewById(R.id.tv_itemEvent_desc);
            imageView = (ImageView) v.findViewById(R.id.iv_itemEvent);
        }
    }

    @Override
    public EventAdapter.ClientViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_event, viewGroup, false);
        EventAdapter.ClientViewHolder pvh = new EventAdapter.ClientViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final EventAdapter.ClientViewHolder listViewHolder, final int position) {
        final ModelEvent modelEvent = (ModelEvent) Client.get(position);
        Glide.with(context).load(AppConfig.IP+modelEvent.getImage()).into(listViewHolder.imageView);
        listViewHolder.tvTitle.setText(modelEvent.getTittle());
        listViewHolder.tvDescription.setText(modelEvent.getDescription());
        Log.d("id",Integer.toString(modelEvent.getId()));
        listViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetailEventActivity.class);
                i.putExtra("id",Integer.toString(modelEvent.getId()));
                i.putExtra("title", modelEvent.getTittle());
                i.putExtra("description", modelEvent.getDescription());
                context.startActivity(i);
                ((Activity)context).overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(Client==null)
            return 0;
        else
            return Client.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
