package com.example.hp.calendra;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Reaper on 12/10/2017.
 */

public class Pometera {

    Context context;
    Gson gson;
    String token, tujuan, content;
    public Pometera(Context context){
        this.context = context;
        gson = new GsonBuilder().create();
    }

    public void sendMessage(String tujuan,String content){
        this.tujuan = tujuan;
        this.content = content;
        RequestQueue queue = Volley.newRequestQueue(context);
        String url ="https://api.pometera.id/smsnotif/token";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("Response",response);
                        PometraActivity.Respon respon = gson.fromJson(response,PometraActivity.Respon.class);
                        token = respon.getAccess_token();
                        sendMessage();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Response Error",error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> params = new HashMap<>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                params.put("X-Pometera-Api-Key","f1f8f4bf-b777-4cae-bc3f-204f1bcecc75");
                return params;
            }
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params = new HashMap<>();
                params.put("grant_type","client_credentials");
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public class Respon{
        String scope;
        String token_type;
        String expire_in;
        String access_token;

        public String getScope() {
            return scope;
        }

        public void setScope(String scope) {
            this.scope = scope;
        }

        public String getToken_type() {
            return token_type;
        }

        public void setToken_type(String token_type) {
            this.token_type = token_type;
        }

        public String getExpire_in() {
            return expire_in;
        }

        public void setExpire_in(String expire_in) {
            this.expire_in = expire_in;
        }

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }
    }

    public void sendMessage(){
        RequestQueue queue = Volley.newRequestQueue(context);
        String url ="https://api.pometera.id/smsnotif/messages";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("Response",response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Response Error",error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> params = new HashMap<>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                params.put("X-Pometera-Api-Key","f1f8f4bf-b777-4cae-bc3f-204f1bcecc75");
                params.put("Accept","application/json");
                params.put("Authorization","Bearer "+token);
                return params;
            }
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params = new HashMap<>();
                params.put("msisdn",tujuan);
                params.put("content",content);
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
