package com.example.hp.calendra.Adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hp.calendra.Model.Timeline;
import com.example.hp.calendra.R;

import java.util.List;

/**
 * Created by Reaper on 12/4/2017.
 */


public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.ClientViewHolder> {

    private List Client;
    final Context context;
    private MediaPlayer mediaPlayer = new MediaPlayer();

    public TimelineAdapter(List<Timeline> listCart, Context context) {
        this.Client = (List) listCart;
        this.context = (Context) context;
    }



    public class ClientViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle,tvTanggal;
        LinearLayout lv;

        ClientViewHolder(View v) {
            super(v);
            tvTitle = v.findViewById(R.id.tv_itemEvent_title);
            tvTanggal = v.findViewById(R.id.tv_itemEvent_tanggal);
        }
    }

    @Override
    public TimelineAdapter.ClientViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_timeline, viewGroup, false);
        TimelineAdapter.ClientViewHolder pvh = new TimelineAdapter.ClientViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final TimelineAdapter.ClientViewHolder listViewHolder, final int position) {
        final Timeline song = (Timeline) Client.get(position);
        listViewHolder.tvTitle.setText(song.getName());
        listViewHolder.tvTanggal.setText(song.getTanggal());
    }

    @Override
    public int getItemCount() {
        if(Client==null){
            return 0;
        }else{
            return Client.size();
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
