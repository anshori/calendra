package com.example.hp.calendra.Adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hp.calendra.Model.Ticket;
import com.example.hp.calendra.R;

import java.util.List;

/**
 * Created by Reaper on 12/4/2017.
 */


public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.ClientViewHolder> {

    private List Client;
    final Context context;
    private MediaPlayer mediaPlayer = new MediaPlayer();

    public TicketAdapter(List<Ticket> listCart, Context context) {
        this.Client = (List) listCart;
        this.context = (Context) context;
    }



    public class ClientViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvPrice;
        LinearLayout lv;

        ClientViewHolder(View v) {
            super(v);
            tvName = v.findViewById(R.id.tv_itemTicket_name);
            tvPrice = v.findViewById(R.id.tv_itemTicket_price);

        }
    }

    @Override
    public TicketAdapter.ClientViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_ticket, viewGroup, false);
        TicketAdapter.ClientViewHolder pvh = new TicketAdapter.ClientViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final TicketAdapter.ClientViewHolder listViewHolder, final int position) {
        final Ticket song = (Ticket) Client.get(position);
        listViewHolder.tvName.setText(song.getName());
        listViewHolder.tvPrice.setText(song.getPrice());
    }

    @Override
    public int getItemCount() {
        if(Client==null){
            return 0;
        }else{
            return Client.size();
        }

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
