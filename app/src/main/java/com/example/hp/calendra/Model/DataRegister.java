package com.example.hp.calendra.Model;

/**
 * Created by hp on 10/12/2017.
 */

public class DataRegister {
    private String email, name, phone, password, id_user;
    private int id;

    public DataRegister(String email, String name, String phone, String password, String id_user, int id) {
        this.email = email;
        this.name = name;
        this.phone = phone;
        this.password = password;
        this.id_user = id_user;
        this.id = id;
    }

    public DataRegister() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
