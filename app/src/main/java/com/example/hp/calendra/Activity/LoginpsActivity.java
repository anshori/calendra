package com.example.hp.calendra.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hp.calendra.Helper.AppConfig;
import com.example.hp.calendra.Helper.SharedPref;
import com.example.hp.calendra.Model.DataLogin;
import com.example.hp.calendra.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

public class LoginpsActivity extends AppCompatActivity {
    EditText editTextUsername, editTextPassword;
    ProgressBar progressBar;
    RequestQueue requestQueue;
    ProgressDialog progressDialog;
    Button btnLoginCostumer;
    Gson gson;
    SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginps);

        sharedPref = new SharedPref(this);
        String uname = sharedPref.loadData("username");
        String password = sharedPref.loadData("password");
        if (!uname.equalsIgnoreCase("")){
            startActivity(new Intent(this, MainActivity.class));
        }
        progressBar = (ProgressBar) findViewById(R.id.progressBarPubliser);
        editTextUsername = (EditText) findViewById(R.id.editTextUsernamePubliser);
        editTextPassword = (EditText) findViewById(R.id.editTextPasswordPubliser);

        progressDialog=new ProgressDialog(LoginpsActivity.this);
        progressDialog.setMessage("Mohon Bersabar...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgress(0);
        progressDialog.setCanceledOnTouchOutside(false);

        findViewById(R.id.buttonLoginPubliser).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                publisherLogin();
            }
        });

        findViewById(R.id.textViewRegisterPubliser).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(getApplicationContext(), RegisterpsActivity.class));
            }
        });
    }

    private void publisherLogin() {
        final String username = editTextUsername.getText().toString();
        final String password = editTextPassword.getText().toString();

        //validating inputs
        if (TextUtils.isEmpty(username)) {
            editTextUsername.setError("Mohon masukkan username");
            editTextUsername.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            editTextPassword.setError("Mohon masukkan password");
            editTextPassword.requestFocus();
            return;
        }

        requestQueue = Volley.newRequestQueue(LoginpsActivity.this);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();

        String url = AppConfig.logincustomer;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        progressDialog.hide();
                        LoginpsActivity.ResponLoginPublisher posts = new ResponLoginPublisher();
                        try{
                            posts =  gson.fromJson(response, LoginpsActivity.ResponLoginPublisher.class);
                        }catch(Exception e){
                            Log.d("lihatjson", "Gson gagal");
                            Toast.makeText(LoginpsActivity.this, "Wrong Username/Password !!!!", Toast.LENGTH_SHORT).show();
                        }
                        Log.d("lihatjson", "onResponse: "+posts.getStatus());
                        if(posts.getStatus().equalsIgnoreCase("success")){
                            sharedPref.saveData("username",username);
                            sharedPref.saveData("password" ,password);
                            Intent i = new Intent(LoginpsActivity.this, MainActivity.class);
                            startActivity(i);
                        }else{
                            Toast.makeText(LoginpsActivity.this, "Tidak bisa login", Toast.LENGTH_SHORT).show();
                            Toast.makeText(LoginpsActivity.this, response, Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Response", error.toString());
                        Toast.makeText(LoginpsActivity.this, "error listener", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", editTextUsername.getText().toString());
                params.put("password", editTextPassword.getText().toString());
//                String username = editTextUsername.getText().toString();
//                String password = editTextPassword.getText().toString();
//
//                User users = new User();
//                users.setUsername(username);
//                users.setPassword(password);
                return params;
            }
        };
        requestQueue.add(postRequest);
    }

    public class ResponLoginPublisher{
        private String status;
        private String message;
        private DataLogin data;

        public ResponLoginPublisher(String status, String message, DataLogin data) {
            this.status = status;
            this.message = message;
            this.data = data;
        }

        public ResponLoginPublisher() {
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public DataLogin getData() {
            return data;
        }

        public void setData(DataLogin data) {
            this.data = data;
        }
    }
}
